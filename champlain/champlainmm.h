/* Copyright (c) 2009  Debarshi Ray <debarshir@src.gnome.org>
 *
 * This file is part of libchamplainmm.
 *
 * libchamplainmm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libchamplainmm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CHAMPLAINMM_H_INCLUDED
#define CHAMPLAINMM_H_INCLUDED

#include <champlainmmconfig.h>

#include <champlainmm/init.h>
#include <champlainmm/adjustment.h>
#include <champlainmm/bounding-box.h>
#include <champlainmm/coordinate.h>
#include <champlainmm/debug.h>
#include <champlainmm/defines.h>
#include <champlainmm/error-tile-renderer.h>
#include <champlainmm/file-cache.h>
#include <champlainmm/file-tile-source.h>
#include <champlainmm/image-renderer.h>
#include <champlainmm/kinetic-scroll-view.h>
#include <champlainmm/label.h>
#include <champlainmm/layer.h>
#include <champlainmm/license.h>
#include <champlainmm/location.h>
#include <champlainmm/map-source.h>
#include <champlainmm/map-source-chain.h>
#include <champlainmm/map-source-desc.h>
#include <champlainmm/map-source-factory.h>
#include <champlainmm/marker.h>
#include <champlainmm/marker-layer.h>
#include <champlainmm/memory-cache.h>
#include <champlainmm/network-tile-source.h>
#include <champlainmm/network-bbox-tile-source.h>
#include <champlainmm/null-tile-source.h>
#include <champlainmm/path-layer.h>
#include <champlainmm/point.h>
#include <champlainmm/renderer.h>
#include <champlainmm/scale.h>
#include <champlainmm/tile.h>
#include <champlainmm/tile-cache.h>
#include <champlainmm/tile-source.h>
#include <champlainmm/view.h>
#include <champlainmm/viewport.h>

#endif /* !CHAMPLAINMM_H_INCLUDED */
