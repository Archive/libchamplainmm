/* Copyright (c) 2014  Juan R. García Blanco <juanrgar@gmail.com>
 *
 * This file is part of libchamplainmm.
 *
 * libchamplainmm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libchamplainmm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "init.h"
#include "wrap_init.h"
#include <glibmm/init.h>
#include <cluttermm/init.h>

namespace Champlain
{

void init(int *argc, char **argv[])
{
  static bool is_initialized = false;

  if (!is_initialized)
  {
    Glib::init();
    Clutter::init(argc, argv);
    wrap_init();

    is_initialized = true;
  }
}

} // namespace Champlain
