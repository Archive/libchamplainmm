/* Copyright (c) 2014  Juan R. García Blanco <juanrgar@gmail.com>
 *
 * This file is part of libchamplainmm.
 *
 * libchamplainmm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libchamplainmm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <champlainmm/adjustment.h>
#include <cluttermm/actor.h>

_DEFS(champlainmm,champlain)
_PINCLUDE(cluttermm/private/actor_p.h)

namespace Champlain
{

/**
 */
class Viewport : public Clutter::Actor
{
  _CLASS_GOBJECT(Viewport, ChamplainViewport, CHAMPLAIN_VIEWPORT, Clutter::Actor, ClutterActor)

protected:
  _CTOR_DEFAULT

public:
  _WRAP_CREATE()

  _WRAP_METHOD(void set_origin(double x, double y), champlain_viewport_set_origin)
  _WRAP_METHOD(void get_origin(double& x, double& y) const, champlain_viewport_get_origin)

  _WRAP_METHOD(void stop(), champlain_viewport_stop)

  _WRAP_METHOD_DOCS_ONLY(champlain_viewport_get_adjustments)
  void get_adjustments(Glib::RefPtr<Adjustment>& hadjustment, Glib::RefPtr<Adjustment>& vadjustment) const;
  _WRAP_METHOD(void set_adjustments(const Glib::RefPtr<Adjustment>& hadjustment, const Glib::RefPtr<Adjustment>& vadjustment), champlain_viewport_set_adjustments)

  _WRAP_METHOD(void set_child(const Glib::RefPtr<Clutter::Actor>& child), champlain_viewport_set_child)

  _WRAP_METHOD(void get_anchor(int& x, int& y) const, champlain_viewport_get_anchor)

  _WRAP_METHOD(void set_actor_position(const Glib::RefPtr<Clutter::Actor>& actor, double x, double y), champlain_viewport_set_actor_position)

  _WRAP_PROPERTY("x-origin", int)
  _WRAP_PROPERTY("y-origin", int)
  _WRAP_PROPERTY("hadjustment", Glib::RefPtr<Adjustment>)
  _WRAP_PROPERTY("vadjustment", Glib::RefPtr<Adjustment>)

  _WRAP_SIGNAL(void relocated(), "relocated", no_default_handler)
};

} // namespace Champlain
