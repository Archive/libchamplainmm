/* Copyright (c) 2014  Juan R. García Blanco <juanrgar@gmail.com>
 *
 * This file is part of libchamplainmm.
 *
 * libchamplainmm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libchamplainmm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtkmm.h>
#include <cluttermm.h>
#include <champlainmm-gtk.h>

class EmbedDemoWindow : public Gtk::Window
{
public:
  EmbedDemoWindow();
  virtual ~EmbedDemoWindow();

protected:
private:
  Gtk::HeaderBar m_header_bar;
  Gtk::Grid m_grid;
  Gtk::SearchEntry m_search_entry;
  Champlain::Gtk::Embed m_champlain_embed;
};

EmbedDemoWindow::EmbedDemoWindow()
{
  // Window properties
  set_title("ChamplainEmbed demo");
  set_default_size(800, 600);
  set_border_width(6);

  // Header bar
  m_header_bar.set_custom_title(m_search_entry);
  m_header_bar.set_show_close_button();
  set_titlebar(m_header_bar);

  // Search entry
  m_search_entry.set_size_request(600, -1);

  // Layout
  m_grid.set_row_spacing(6);
  m_grid.attach(m_champlain_embed, 0, 0, 1, 1);
  add(m_grid);

  show_all_children();
}

EmbedDemoWindow::~EmbedDemoWindow()
{
}

int main(int argc, char *argv[])
{
  Champlain::Gtk::init(&argc, &argv);

  Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "org.libchamplainmm.demo");

  EmbedDemoWindow window;

  return app->run(window);
}
