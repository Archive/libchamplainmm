/* Copyright (c) 2009  Debarshi Ray <debarshir@src.gnome.org>
 *
 * This file is part of libchamplainmm.
 *
 * libchamplainmm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libchamplainmm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

#include <glibmm_generate_extra_defs/generate_extra_defs.h>
#include <champlain/champlain.h>
#include <champlain/champlain-adjustment.h>
#include <champlain/champlain-viewport.h>
#include <champlain/champlain-kinetic-scroll-view.h>

int
main(int argc, char *argv[])
{
  clutter_init(&argc, &argv);

  std::cout << get_defs(CHAMPLAIN_TYPE_ADJUSTMENT)
            << get_defs(CHAMPLAIN_TYPE_BOUNDING_BOX)
            << get_defs(CHAMPLAIN_TYPE_COORDINATE)
            << get_defs(CHAMPLAIN_TYPE_ERROR_TILE_RENDERER)
            << get_defs(CHAMPLAIN_TYPE_FILE_CACHE)
            << get_defs(CHAMPLAIN_TYPE_FILE_TILE_SOURCE)
            << get_defs(CHAMPLAIN_TYPE_IMAGE_RENDERER)
            << get_defs(CHAMPLAIN_TYPE_KINETIC_SCROLL_VIEW)
            << get_defs(CHAMPLAIN_TYPE_LABEL)
            << get_defs(CHAMPLAIN_TYPE_LAYER)
            << get_defs(CHAMPLAIN_TYPE_LICENSE)
            << get_defs(CHAMPLAIN_TYPE_LOCATION)
            << get_defs(CHAMPLAIN_TYPE_MAP_SOURCE)
            << get_defs(CHAMPLAIN_TYPE_MAP_SOURCE_DESC)
            << get_defs(CHAMPLAIN_TYPE_MAP_SOURCE_CHAIN)
            << get_defs(CHAMPLAIN_TYPE_MARKER_LAYER)
            << get_defs(CHAMPLAIN_TYPE_MARKER)
            << get_defs(CHAMPLAIN_TYPE_MEMORY_CACHE)
            << get_defs(CHAMPLAIN_TYPE_NETWORK_TILE_SOURCE)
            << get_defs(CHAMPLAIN_TYPE_NETWORK_BBOX_TILE_SOURCE)
            << get_defs(CHAMPLAIN_TYPE_NULL_TILE_SOURCE)
            << get_defs(CHAMPLAIN_TYPE_PATH_LAYER)
            << get_defs(CHAMPLAIN_TYPE_POINT)
            << get_defs(CHAMPLAIN_TYPE_RENDERER)
            << get_defs(CHAMPLAIN_TYPE_SCALE)
            << get_defs(CHAMPLAIN_TYPE_TILE)
            << get_defs(CHAMPLAIN_TYPE_TILE_CACHE)
            << get_defs(CHAMPLAIN_TYPE_TILE_SOURCE)
            << get_defs(CHAMPLAIN_TYPE_VIEW)
            << get_defs(CHAMPLAIN_TYPE_VIEWPORT)
            ;

  return 0;
}
