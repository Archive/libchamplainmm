dnl Include gmmproc type conversion definitions:
m4_include(`convert_base.m4')
m4_include(`convert_glib.m4')
m4_include(`convert_pango.m4')
m4_include(`convert_clutter.m4')
m4_include(`convert_libchamplain.m4')
m4_include(`class_gtkobject.m4')
