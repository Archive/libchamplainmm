# Actor
_CONVERSION(`ClutterActor*',`Glib::RefPtr<const Clutter::Actor>',`Glib::wrap($3)')
_CONVERSION(`ClutterActor*',`Glib::RefPtr<Clutter::Actor>',`Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<Clutter::Actor>&',`ClutterActor*',__CONVERT_REFPTR_TO_P)

# Color
_CONVERSION(`ClutterColor*', `Clutter::Color', `Clutter::Color($3, true)')
_CONVERSION(`const Clutter::Color&',`const ClutterColor*',__FR2P)
_CONVERSION(`Clutter::Color',`ClutterColor*',__FR2P)

# Content
_CONVERSION(`const Glib::RefPtr<Clutter::Content>&',`ClutterContent*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`ClutterContent*',`Glib::RefPtr<Clutter::Content>',`Glib::wrap($3)')
_CONVERSION(`ClutterContent*',`Glib::RefPtr<const Clutter::Content>',`Glib::wrap($3)')

# Event
_CONVERSION(`Clutter::Event&',`ClutterEvent*',`&$3')
