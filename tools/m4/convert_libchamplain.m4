dnl Copyright (c) 2009  Debarshi Ray <debarshir@src.gnome.org>
dnl This file is part of libchamplainmm.

# enums
_CONV_ENUM(Champlain,MapProjection)
_CONV_ENUM(Champlain,ScrollMode)
_CONV_ENUM(Champlain,SelectionMode)
_CONV_ENUM(Champlain,State)
_CONV_ENUM(Champlain,Unit)
_CONV_ENUM(Pango,EllipsizeMode)

# Glib
_CONVERSION(`const GTimeVal*',`Glib::TimeVal',`$2(*$3)')
_CONVERSION(`const Glib::TimeVal&', `const GTimeVal*', static_cast<$2>(&$3))

# Adjustment
_CONVERSION(`const Glib::RefPtr<Adjustment>&',`ChamplainAdjustment*',__CONVERT_REFPTR_TO_P)

# BaseMarker
_CONVERSION(`ChamplainBaseMarker*',`Glib::RefPtr<BaseMarker>',`Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<BaseMarker>&',`ChamplainBaseMarker*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`const Glib::ArrayHandle<Glib::RefPtr<BaseMarker> >&',`ChamplainBaseMarker*[]',`const_cast<ChamplainBaseMarker**>(($3).data())')
_CONVERSION(`const GList*',`Glib::ListHandle<Glib::RefPtr<BaseMarker> >',`$2(const_cast<GList*>($3),Glib::OWNERSHIP_NONE)')

# BoundingBox
_CONVERSION(`BoundingBox&',`ChamplainBoundingBox*',`($3).gobj()')
_CONVERSION(`const BoundingBox&',`ChamplainBoundingBox*',`const_cast<ChamplainBoundingBox*>(($3).gobj())')
_CONVERSION(`BoundingBox',`ChamplainBoundingBox*',`($3).gobj()')
_CONVERSION(`ChamplainBoundingBox*',`BoundingBox',`Glib::wrap($3)')

# Cache
_CONVERSION(`ChamplainCache*',`Glib::RefPtr<Cache>',`Glib::wrap($3)')

# Layer
_CONVERSION(`const Glib::RefPtr<Layer>&',`ChamplainLayer*',__CONVERT_REFPTR_TO_P)

# License
_CONVERSION(`ChamplainLicense*',`Glib::RefPtr<License>', `Glib::wrap($3)')

# Location
_CONVERSION(`const Glib::RefPtr<Location>&',`ChamplainLocation*',__CONVERT_REFPTR_TO_P)

# MapSource
_CONVERSION(`ChamplainMapSource*',`Glib::RefPtr<MapSource>',`Glib::wrap($3)')
_CONVERSION(`ChamplainMapSource*',`Glib::RefPtr<const MapSource>',`Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<MapSource>&',`ChamplainMapSource*',__CONVERT_REFPTR_TO_P)

# MapSourceDesc
_CONVERSION(`const Glib::RefPtr<MapSourceDesc>&',`ChamplainMapSourceDesc*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`ChamplainMapSourceDesc*',`Glib::RefPtr<MapSourceDesc>',`Glib::wrap($3)')
_CONVERSION(`GSList*',`Glib::SListHandle<const MapSourceDesc *>',__FL2H_SHALLOW)

# MapSourceFactory
_CONVERSION(`ChamplainMapSourceFactory*',`Glib::RefPtr<MapSourceFactory>',`Glib::wrap($3)')

# Marker
_CONVERSION(`const Glib::RefPtr<Marker>&',`ChamplainMarker*',__CONVERT_REFPTR_TO_P)

# Point
_CONVERSION(`GList*',`Glib::ListHandle<const Point *>',__FL2H_SHALLOW)
_CONVERSION(`const Point&',`ChamplainPoint*',__FCR2P)

# Polygon
_CONVERSION(`const Glib::RefPtr<Polygon>&',`ChamplainPolygon*',__CONVERT_REFPTR_TO_P)

# Renderer
_CONVERSION(`ChamplainRenderer*',`Glib::RefPtr<Renderer>',`Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<Renderer>&',`ChamplainRenderer*',__CONVERT_REFPTR_TO_P)

# Tile
_CONVERSION(`ChamplainTile*',`const Glib::RefPtr<Tile>&',`Glib::wrap($3)')
_CONVERSION(`ChamplainTile*',`Glib::RefPtr<Tile>',`Glib::wrap($3)')
_CONVERSION(`ChamplainTile*',`Glib::RefPtr<const Tile>',`Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<Tile>&',`ChamplainTile*',__CONVERT_REFPTR_TO_P($3))

# TileCache
_CONVERSION(`const Glib::RefPtr<TileCache>&',`ChamplainTileCache*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`ChamplainTileCache*',`Glib::RefPtr<TileCache>',`Glib::wrap($3)')

# View
_CONVERSION(`const Glib::RefPtr<View>&',`ChamplainView*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`ChamplainView*',`const Glib::RefPtr<View>&',`Glib::wrap($3)')

# Viewport
_CONVERSION(`const Glib::RefPtr<Viewport>&',`ChamplainViewport*',__CONVERT_REFPTR_TO_P)
